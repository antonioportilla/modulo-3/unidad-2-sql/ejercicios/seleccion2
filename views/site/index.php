<?php
use yii\bootstrap\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1>consultas de seleccion 2</h1>
Application
        <p class="lead">Modulo 3 Unidad 2</p>
    </div>

    <div class="body-content">

        <div class="row">
            <!-- primera consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 1</h3>
                      <p>Número de ciclistas que hay</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta1a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta1'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
            <!-- segunda consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 2</h3>
                      <p>Número de ciclistas que hay en el equipo Banesto</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta2a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta2'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
             <!-- tercera consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 3</h3>
                      <p>La edad media de los ciclistas</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta3a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta3'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
             
              <!-- cuarta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 4</h3>
                      <p>La edad media de los ciclistas de Banesto</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta4a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta4'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
               <!-- quinta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 5</h3>
                      <p>La edad media de los ciclists por cada equipo</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta5a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta5'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
               <!-- sexta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 6</h3>
                      <p>El número de ciclistas por equipo</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta6a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta6'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
               
              <!-- septima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 7</h3>
                      <p>El número total de puertos</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta7a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta7'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              <!-- octava consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 8</h3>
                      <p>El número total de puertos mayores de 1500</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta8a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta8'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              <!-- novena consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 9</h3>
                      <p>Listas nombre de los equipos de mas de 4 ciclistas</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta9a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta9'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              <!-- decima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 10</h3>
                      <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta10a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta10'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              <!-- undecima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 11</h3>
                      <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta11a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta11'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- duodecima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 12</h3>
                      <p>Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta12a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta12'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
        </div>

    </div>
</div>
