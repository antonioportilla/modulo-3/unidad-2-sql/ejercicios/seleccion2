﻿USE ciclistas;

-- 1.1 Número de ciclistas que hay.
SELECT COUNT(*) AS nciclistas FROM ciclista;


-- 1.2 Número de ciclistas que hay en el equipo Banesto.
SELECT COUNT(*) AS nciclistas FROM ciclista WHERE nomequipo='banesto';

-- 1.3 La edad media de los ciclistas.
SELECT avg(edad) edadMedia FROM ciclista;

-- 1.4 La edad media de los ciclistas de Banesto.
SELECT avg(edad) edadMedia FROM ciclista WHERE nomequipo='Banesto';

-- 1.5 La edad media de los ciclists por cada equipo;
SELECT nomequipo, avg(edad) edadMedia FROM ciclista GROUP BY nomequipo;

-- 1.6 El número de ciclistas por equipo.
SELECT nomequipo, count(*) nCiclistas  FROM ciclista GROUP BY nomequipo;

-- 1.7 El número total de puertos.
SELECT COUNT(*) npuertos FROM puerto;

SELECT * FROM puerto limit 1 ;

-- 1.8 El número total de puertos mayores de 1500.
SELECT COUNT(*) npuertos FROM puerto WHERE altura>1500;

-- 1.9 Listas nombre de los equipos de mas de 4 ciclistas.
SELECT nomequipo, count(*) nciclistas FROM ciclista GROUP BY nomequipo HAVING nciclistas>4;

-- 1.10 Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32.
SELECT nomequipo, count(*) n FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING n>4;

-- 1.11 Indícame el número de etapas que ha ganado cada uno de los ciclistas.
SELECT ciclista.*, c1.veces_ganadas FROM ciclista left JOIN
(SELECT dorsal,COUNT(*) veces_ganadas FROM etapa GROUP BY dorsal) c1
USING(dorsal);
  

-- 1.12 Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa.

SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*)>1;