<?php

namespace app\controllers;
use yii\data\SqlDataProvider;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /*
     * Creado esta accion
     */
    
    public function actionCrud(){
        return $this->render('gestion');
    }
    
 
    public function actionConsulta1a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("COUNT(*) AS nciclistas"),
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nciclistas'],
            "titulo"=>"Consulta 1",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) AS nciclistas FROM ciclista",
        ]);
    }
    
    public function actionConsulta1(){
            // mediante DAO
        
        $dataProvider= new SqlDataProvider([
            'sql'=> 'SELECT COUNT(*) AS nciclistas FROM ciclista' ,
            
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nciclistas'],
            "titulo"=>"Consulta 1",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) AS nciclistas FROM ciclista",
        ]);
                
    }
    
    public function actionConsulta2a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("COUNT(*) AS nciclistas")->distinct()->where("nomequipo='banesto'"),
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nciclistas'],
            "titulo"=>"Consulta 2",
            "enunciado"=>"Número de ciclistas que hay en el equipo Banesto",
            "sql"=>"SELECT COUNT(*) AS nciclistas FROM ciclista WHERE nomequipo='banesto'",
        ]);
    }
    
     public function actionConsulta2(){
            // mediante DAO
        
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT COUNT(*) AS nciclistas FROM ciclista WHERE nomequipo='banesto'" ,
                'pagination'=>[
                'pageSize'=>0,
                ],
            
            
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nciclistas'],
            "titulo"=>"Consulta 2",
            "enunciado"=>"Número de ciclistas que hay en el equipo Banesto",
            "sql"=>"SELECT COUNT(*) AS nciclistas FROM ciclista WHERE nomequipo='banesto'",
        ]); 
        
    }
    
    public function actionConsulta3a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("avg(edad) edadMedia"),
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edadMedia'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"La edad media de los ciclistas",
            "sql"=>"SELECT avg(edad) edadMedia FROM ciclista",
        ]);
    }
    
     public function actionConsulta3(){
            // mediante DAO
               
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT avg(edad) edadMedia FROM ciclista" ,
                'pagination'=>[
                'pageSize'=>0,
                ],
            
            
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edadMedia'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"La edad media de los ciclistas",
            "sql"=>"SELECT avg(edad) edadMedia FROM ciclista",
        ]); 
    }
    
    public function actionConsulta4a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("avg(edad) edadMedia")
                ->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edadMedia'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"La edad media de los ciclistas de Banesto",
            "sql"=>"SELECT avg(edad) edadMedia FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
     public function actionConsulta4(){
            // mediante DAO
        
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT avg(edad) edadMedia FROM ciclista WHERE nomequipo='Banesto'" ,
        
            'pagination'=>[
                'pageSize'=>0,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edadMedia'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"La edad media de los ciclistas de Banesto",
            "sql"=>"SELECT avg(edad) edadMedia FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
       public function actionConsulta5a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("nomequipo, avg(edad) edadMedia")
                ->groupBy("nomequipo"),
            'pagination'=>[
                'pageSize'=>5,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edadMedia'],
            "titulo"=>"Consulta 5",
            "enunciado"=>"La edad media de los ciclists por cada equipo",
            "sql"=>"SELECT nomequipo, avg(edad) edadMedia FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
     public function actionConsulta5(){
            // mediante DAO
        
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT nomequipo, avg(edad) edadMedia FROM ciclista GROUP BY nomequipo" ,
        
            'pagination'=>[
                'pageSize'=>5,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','edadMedia'],
            "titulo"=>"Consulta 5",
            "enunciado"=>"La edad media de los ciclists por cada equipo",
            "sql"=>"SELECT nomequipo, avg(edad) edadMedia FROM ciclista GROUP BY nomequipo",
        ]);
    } 
    
     public function actionConsulta6a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("nomequipo, count(*) nciclistas")
                ->groupBy("nomequipo"),
            'pagination'=>[
                'pageSize'=>30,
                ]
        ]);
                
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','nciclistas'],
            "titulo"=>"Consulta 6",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, count(*) nCiclistas  FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
     public function actionConsulta6(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(*) FROM ciclista GROUP BY nomequipo" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT nomequipo, count(*) nciclistas  FROM ciclista GROUP BY nomequipo" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>30,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','nciclistas'],
            "titulo"=>"Consulta 6",
            "enunciado"=>"Listar el numero de ciclistas por equipo",
            "sql"=>"SELECT nomequipo, count(*) nCiclistas  FROM ciclista GROUP BY nomequipo",
        ]);
    } 
    
    
    public function actionConsulta7a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()->select("COUNT(*) npuertos")->distinct(),
            'pagination'=>[
                'pageSize'=>30,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['npuertos'],
            "titulo"=>"Consulta 7",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) npuertos FROM puerto",
        ]);
    }
    
     public function actionConsulta7(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT 1 ")
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT COUNT(*) npuertos FROM puerto" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>30,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['npuertos'],
            "titulo"=>"Consulta 7",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) npuertos FROM puerto",
        ]);
    } 
    
    public function actionConsulta8a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()->select("COUNT(*) npuertos")->distinct()->where ("altura>1500")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['npuertos'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) npuertos FROM puerto WHERE altura>1500",
        ]);
    }
    
     public function actionConsulta8(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT 1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT COUNT(*) npuertos FROM puerto WHERE altura>1500" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['npuertos'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) npuertos FROM puerto WHERE altura>1500",
        ]);
    } 
    
    public function actionConsulta9a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("nomequipo, count(*) nciclistas")->groupBy ("nomequipo")->having("nciclistas>4")
                ,
            'pagination'=>[
                'pageSize'=>3,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','nciclistas'],
            "titulo"=>"Consulta 9",
            "enunciado"=>"Listas nombre de los equipos de mas de 4 ciclistas",
            "sql"=>"SELECT nomequipo, count(*) nciclistas FROM ciclista GROUP BY nomequipo HAVING nciclistas>4",
        ]);
    }
    
     public function actionConsulta9(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(*) nciclistas FROM ciclista GROUP BY nomequipo HAVING nciclistas>4" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT nomequipo, count(*) nciclistas FROM ciclista GROUP BY nomequipo HAVING nciclistas>4" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>3,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','nciclistas'],
            "titulo"=>"Consulta 9",
            "enunciado"=>"Listas nombre de los equipos de mas de 4 ciclistas",
            "sql"=>"SELECT nomequipo, count(*) nciclistas FROM ciclista GROUP BY nomequipo HAVING nciclistas>4",
        ]);
    } 
    
     public function actionConsulta10a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("nomequipo")->where(["between","edad", '28', '32'])->groupBy("nomequipo")->having("count(*)>4")
                ,
            'pagination'=>[
                'pageSize'=>3,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32",
            "sql"=>"SELECT nomequipo, count(*) nciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING nciclistas>4",
        ]);
    }
    
     public function actionConsulta10(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT  count(*) nciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING nciclistas>4" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT nomequipo, count(*) nciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING nciclistas>4" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>3,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','nciclistas'],
            "titulo"=>"Consulta 10",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32",
            "sql"=>"SELECT nomequipo, count(*) nciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING nciclistas>4",
        ]);
    }
    
    public function actionConsulta11a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()->select("dorsal, COUNT(*) netapas")->groupBy("dorsal")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','netapas'],
            "titulo"=>"Consulta 11",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) netapas FROM etapa GROUP BY dorsal",
        ]);
    }
    
     public function actionConsulta11(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(distinct dorsal) FROM etapa" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT dorsal, COUNT(*) netapas FROM etapa GROUP BY dorsal" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','netapas'],
            "titulo"=>"Consulta 11",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT dorsal, COUNT(*) netapas FROM etapa GROUP BY dorsal",
        ]);
    }
    
     public function actionConsulta12a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()->select("dorsal")->groupBy ("dorsal")->having("count(*)>1")
                ,
            'pagination'=>[
                'pageSize'=>4,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*)>1",
        ]);
    }
    
     public function actionConsulta12(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(*) FROM (SELECT count(*) FROM etapa GROUP BY dorsal HAVING COUNT(*)>1)c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*)>1" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>4,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING COUNT(*)>1",
        ]);
    }
    
    
    
    
     }
